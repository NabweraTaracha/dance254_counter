from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
	url(r'^admin/', include(admin.site.urls)),
	url(r'', include('counter.urls')),

	# url(r'^$', 'counter.views.home', name='home'),
    # Examples:
    # url(r'^$', 'dance254_counter.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    
)
