from django import forms

from .models import Post, Email


class PostForm(forms.ModelForm):

	class Meta:
		model = Post
		fields = ('title', 'text',)


class EmailForm(forms.ModelForm):

	class Meta:
		model = Email
		fields = ['full_name', 'email']
		# exclude = ['full_name'] use sparingly

	def clean_email(self):
		email = self.cleaned_data.get('email')
		email_base, provider = email.split("@")
		domain, extension = provider.split('.')
		# if not domain == 'ku':
		# 	raise forms.ValidationError("Please make sure you use your KU email.")
		if not extension == "edu":
			raise forms.ValidationError("Please use a valid .EDU email address")
		return email

	def clean_full_name(self):
		full_name = self.cleaned_data.get('full_name')
		# Write Validation code.
		return full_name
