from django.contrib import admin

from .forms import EmailForm
from .models import Post, Email
# from .models import Email


class EmailAdmin(admin.ModelAdmin):
	list_display = ["__unicode__", "timestamp", "updated"]

	form = EmailForm

	# class Meta:
	# 	model = Email

admin.site.register(Post)
admin.site.register(Email, EmailAdmin)
