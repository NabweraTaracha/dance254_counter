from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
	url(r'^$', views.home, name='home'),
	url(r'^love-struck-challenge/$', views.love_struck, name='love_struck'),
	url(r'^international-dance-day/$', views.int_dance_day, name='int_dance_day'),
	# url(r'^$', views.post_list, name='post_list'),
	# url(r'^post/(?P<pk>[0-9]+)/$', views.post_detail, name='post_detail'),
	# url(r'^post/new/$', views.post_new, name='post_new'),
	# url(r'^post/(?P<pk>[0-9]+)/edit/$', views.post_edit, name='post_edit'),
)

# Django 1.8
# urlpatterns = [
#     url(r'^$', views.post_list, name='post_list'),
# ]
