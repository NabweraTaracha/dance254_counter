from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone

from .models import Post
from .forms import EmailForm, PostForm


# HomePage View.
def home(request):
	title = 'Welcome'
	form = EmailForm(request.POST or None)
	context = {
		"title": title,
		"form": form,
	}

	if form.is_valid():
		# form.save()
		print request.POST['email']  # Not recommended, Do not save data from request post as its not validated.
		instance = form.save(commit=False)

		full_name = form.cleaned_data.get("full_name")
		if not full_name:
			full_name = "New Full Name"
		instance.full_name = full_name

		# if not instance.full_name:
		# 	instance.full_name = "Nabwera"
		instance.save()
		context = {
			"title": "Thank You"
		}

	template = "home.html"
	return render(request, template, context)


# Love_Struck Page View
def love_struck(request):
	context = {}
	template = "love_struck.html"
	return render(request, template, context)


# International Dance Day Page View
def int_dance_day(request):
	context = {}
	template = "int_dance_day.html"
	return render(request, template, context)


def post_list(request):
	posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
	template = "post_list.html"
	return render(request, template, {'posts': posts})


def post_detail(request, pk):
	post = get_object_or_404(Post, pk=pk)
	template = "post_detail.html"
	return render(request, template, {'post': post})


def post_new(request):
	if request.method == "POST":
		form = PostForm(request.POST)
		if form.is_valid():
			post = form.save(commit=False)
			post.author = request.user
			post.published_date = timezone.now()
			post.save()
			return redirect('post_detail', pk=post.pk)
	else:
		form = PostForm()
		# template = 'post_edit.html'
	return render(request, 'post_edit.html', {'form': form})


def post_edit(request, pk):
	post = get_object_or_404(Post, pk=pk)
	if request.method == "POST":
		form = PostForm(request.POST, instance=post)
		if form.is_valid():
			post = form.save(commit=False)
			post.author = request.user
			post.published_date = timezone.now()
			post.save()
			return redirect('post_detail', pk=post.pk)
	else:
		form = PostForm(instance=post)
		template = 'post_edit.html'
	return render(request, template, {'form': form})
